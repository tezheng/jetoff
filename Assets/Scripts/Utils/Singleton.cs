
using System;
using System.Collections;

using UnityEngine;

public class Singleton<Type>
	where Type : Singleton<Type>, new()
{
	private static Type instance_;
	static void createInstance()
	{
		instance_ = new Type();
	}
	public static Type Instance
	{
		get
		{
			if (instance_ == null)
				createInstance();
			
			return instance_;
		}
	}
}

public class UnitySingleton<Type>: MonoBehaviour
	where Type : UnitySingleton<Type>, new()
{
	private static Type instance_;
	public static Type Instance
	{
		get
		{
			return instance_;
		}
	}

	protected virtual void Awake()
	{
		instance_ = this as Type;
	}
}
