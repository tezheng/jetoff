﻿
using UnityEngine;

using System;
using System.Collections.Generic;

public class MonoBehaviourEx: MonoBehaviour
{
	public bool ignoreTimeScale = false;
	protected float time { get { return ignoreTimeScale ? RealTime.time : Time.time; } }
	protected float deltaTime { get { return ignoreTimeScale ? RealTime.deltaTime : Time.deltaTime; } }
}

public static class GameObjectVisitor
{
	public delegate bool condition(GameObject obj);
	
	public static GameObject FindChild(this GameObject _this, string name)
	{
		return FindChildInCondition(_this, delegate(GameObject obj) {
			return (obj.name == name);
		});
	}

	public static GameObject FindChildWithNamePrefix(this GameObject _this, string prefix)
	{
		return FindChildInCondition(_this, delegate(GameObject obj) {
			return (obj.name.StartsWith(prefix));
		});
	}

	public static GameObject FindChildWithTag(this GameObject _this, string tag)
	{
		return FindChildInCondition(_this, delegate(GameObject obj) {
			return (obj.tag == tag);
		});
	}

	public static GameObject FindChildInCondition(this GameObject _this, condition cond)
	{
		if (_this == null)
			return null;

		Transform transform = _this.GetComponent<Transform>();
		foreach (Transform trans in transform)
		{
			if (cond(trans.gameObject))
			{
				return trans.gameObject;
			}
			else
			{
				GameObject obj = trans.gameObject.FindChildInCondition(cond);
				if (obj)
					return obj;
			}
		}
		
		return null;
	}

	public static List<GameObject> FindChildren(this GameObject _this, string name)
	{
		List<GameObject> objs = new List<GameObject>();

		FindChildrenInCondition(_this, objs, delegate(GameObject obj) {
			return (obj.name == name);
		});

		return objs;
	}

	public static List<GameObject> FindChildrenWithNamePrefix(this GameObject _this, string prefix)
	{
		List<GameObject> objs = new List<GameObject>();
		
		FindChildrenInCondition(_this, objs, delegate(GameObject obj) {
			return obj.name.StartsWith(prefix);
		});
		
		return objs;
	}

	public static List<GameObject> FindChildrenWithTag(this GameObject _this, string tag)
	{
		List<GameObject> objs = new List<GameObject>();

		FindChildrenInCondition(_this, objs, delegate(GameObject obj) {
			return (obj.tag == tag);
		});

		return objs;
	}

	public static List<T> FindChildrenWithComponent<T>(this GameObject _this) where T: Component
	{
		if (_this == null)
			return null;

		List<T> ret = new List<T>();
		_this.GetComponentsInChildren<T>(includeInactive:true, results:ret);

		return ret;
	}

	public static void FindChildrenInCondition(this GameObject _this, List<GameObject> objs, condition cond)
	{
		if (_this == null)
			return;

		Transform transform = _this.GetComponent<Transform>();
		foreach (Transform trans in transform)
		{
			if (cond(trans.gameObject))
			{
				objs.Add(trans.gameObject);
			}
			
			trans.gameObject.FindChildrenInCondition(objs, cond);
		}
	}

	public static T FindComponentInChildren<T>(this GameObject _this)
		where T: MonoBehaviour
	{
		if (_this == null)
			return null;

		T ret = null;
		FindChildInCondition(_this, (obj) => {
			ret = obj.GetComponent<T>();
			return ret != null;
		});

		return ret;
	}

	public static void VisitChildren(this GameObject _this, Action<GameObject> fn)
	{
		Transform transform = _this.GetComponent<Transform>();
		foreach (Transform trans in transform)
		{
			fn(trans.gameObject);
			trans.gameObject.VisitChildren(fn);
		}
	}

	public static void RemoveAllChildren(this GameObject _this)
	{
		Transform transform = _this.GetComponent<Transform>();
		foreach (Transform trans in transform)
		{
			GameObject.Destroy(trans.gameObject);
		}
	}
}

public static class MonoBehaviourVisitor
{
	public static GameObject FindChild(this MonoBehaviour _this, string name)
	{
		return _this.gameObject.FindChild(name);
	}
	
	public static List<GameObject> FindChildren(this MonoBehaviour _this, string name)
	{
		return _this.gameObject.FindChildren(name);
	}
}

public static class GameObject_MonoBehaviour
{
	public static T SafeGetComponent<T>(this GameObject _this)
		where T: Component
	{
		if (_this == null)
			return null;

		return _this.GetComponent<T>();
	}

	public static T GetOrCreateComponent<T>(this GameObject _this)
		where T: Component
	{
		if (_this == null)
			return null;

		var ret = _this.GetComponent<T>();
		return (ret == null) ? _this.AddComponent<T>() : ret;
	}
}

public static class Component_Extension
{
	public static T SafeGetComponent<T>(this Component _this)
		where T: MonoBehaviour
	{
		return (_this == null) ? null : _this.gameObject.SafeGetComponent<T>();
	}
	
	public static T GetOrCreateComponent<T>(this Component _this)
		where T: MonoBehaviour
	{
		return (_this == null) ? null : _this.gameObject.GetOrCreateComponent<T>();
	}
}

public static class GameObject_Instantiate
{
	public static GameObject Instantiate(this GameObject _this)
	{
		if (_this == null)
			return null;

		return GameObject.Instantiate(_this) as GameObject;
	}

	public static GameObject Instantiate(this GameObject _this, Vector3 position, Quaternion rotation)
	{
		if (_this == null)
			return null;

		GameObject ret = _this.Instantiate();
		ret.transform.localPosition = position;
		ret.transform.localRotation = rotation;

		return ret;
	}
}

public static class GameObject_Hierarchy
{
	public static GameObject ChangeParent(this GameObject _this, GameObject parent, bool resetMat = true)
	{
		_this.transform.ChangeParent(parent.transform, resetMat);
		return _this;
	}

	public static void ChangeParent(this GameObject _this, Transform transform, bool resetMat = true)
	{
		_this.transform.ChangeParent(transform, resetMat);
	}

	public static void ChangeParent(this GameObject _this, GameObject parent, Vector3 localPosition)
	{
		_this.transform.ChangeParent(parent.transform, false);
		_this.transform.localPosition = localPosition;
	}
}

public static class MonoBehaviour_MonoBehaviour
{
	public static T AddComponent<T>(this MonoBehaviour _this)
		where T: MonoBehaviour
	{
		if (_this == null)
			return null;
		
		return _this.gameObject.AddComponent<T>();
	}

	public static T GetComponent<T>(this MonoBehaviour _this)
		where T: MonoBehaviour
	{
		if (_this == null)
			return null;
		
		return _this.gameObject.GetComponent<T>();
	}
}
