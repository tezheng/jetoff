
using UnityEngine;
using System;

public class UnitySubject: MonoBehaviour
{
	private SubjectAggregation container;

	void Awake()
	{
		container = new SubjectAggregation();
	}

	public INotificationHandler<Type> Subscribe<Type>(Action<Type> func)
		where Type: Subject<Type>, new()
	{
		return container.Subscribe<Type>(func);
	}
	
	public bool Subscribe<Type>(INotificationHandler<Type> handler)
		where Type: Subject<Type>, new()
	{
		return container.Subscribe<Type>(handler);
	}

	public bool Unsubscribe<Type>(INotificationHandler<Type> handler)
		where Type: Subject<Type>, new()
	{
		return container.Unsubscribe<Type>(handler);
	}

	public Type Notification<Type>()
		where Type: Subject<Type>, new()
	{
		var notification = container.Notification<Type>();
		if (notification != null)
		{
			return notification;
		}
		else
		{
			// Dummy notification
			Debug.LogWarning("No one subscribes to the notification: " + typeof(Type).Name);
			return new Type();
		}
	}
}

public static class GameObject_UnitySubject
{
	public static Type Notification<Type>(this GameObject _this)
		where Type : Subject<Type>, new()
	{
		if (_this != null)
		{
			UnitySubject nc = _this.GetComponent<UnitySubject>();
			if (nc)
			{
				Type ret = nc.Notification<Type>();
				return ret;
			}
			else
			{
				Debug.LogWarning("[GameObject_UnitySubject] This game object does not have UnitySubject component. "+_this);
			}
		}
		else
		{
			Debug.LogWarning("[GameObject_UnitySubject] The game object is null.");
		}

		// return a dummy instance as fallback, which should not happened under any circumstances, theoretically
		return new Type();
	}

	public static bool Subscribe<Type>(this GameObject _this, INotificationHandler<Type> handler)
		where Type: Subject<Type>, new()
	{
		if (_this != null)
		{
			UnitySubject nc = _this.GetComponent<UnitySubject>();
			if (nc)
			{
				return nc.Subscribe<Type>(handler);
			}
			else
			{
				Debug.LogWarning("[GameObject_UnitySubject] This game object does not have UnitySubject component. "+_this);
				return false;
			}
		}
		else
		{
			Debug.LogWarning("[GameObject_UnitySubject] The game object is null.");
			return false;
		}
	}
	
	public static bool Unsubscribe<Type>(this GameObject _this, INotificationHandler<Type> handler)
		where Type: Subject<Type>, new()
	{
		if (_this != null)
		{
			UnitySubject nc = _this.GetComponent<UnitySubject>();
			if (nc)
			{
				return nc.Unsubscribe<Type>(handler);
			}
			else
			{
				Debug.LogWarning("[GameObject_UnitySubject] This game object does not have UnitySubject component. "+_this);
				return false;
			}
		}
		else
		{
			Debug.LogWarning("[GameObject_UnitySubject] The game object is null.");
			return false;
		}
	}
}

public class NotificationCenter: SubjectAggregation
{
	private static NotificationCenter instance_;
	static void createInstance()
	{
		instance_ = new NotificationCenter();
	}
	public static NotificationCenter Instance
	{
		get
		{
			if (instance_ == null)
				createInstance();
			
			return instance_;
		}
	}
}
