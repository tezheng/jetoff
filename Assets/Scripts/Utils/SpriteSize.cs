﻿using UnityEngine;
using System.Collections;

public class SpriteSize : MonoBehaviour
{
	public float width;
	public float height;

	void Awake()
	{
		var sprite = GetComponent<SpriteRenderer>().sprite;
		width = sprite.rect.width / sprite.pixelsPerUnit;
		height = sprite.rect.height / sprite.pixelsPerUnit;
	}	
}
