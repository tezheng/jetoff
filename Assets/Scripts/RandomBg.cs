﻿using UnityEngine;
using System.Collections;

public class RandomBg : MonoBehaviour 
{
	public Sprite[] spritelist;

	int curindex = -1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChoseRandomBg()
	{
		int index = Random.Range(0,spritelist.Length);
		if(index == curindex)
		{
			index = (curindex + 1) % spritelist.Length;
		}
		gameObject.GetComponent<SpriteRenderer>().sprite = spritelist[index];
		curindex = index;
	}
}
