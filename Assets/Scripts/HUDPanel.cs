﻿using UnityEngine;
using System.Collections;

public class HUDPanel : MonoBehaviour {

	public GameObject startgroup;
	public GameObject ingamegroup;
	public GameObject pausegroup;
	public GameObject endgroup;
	public GameObject pausebutton;
	public GameObject hint;

	public UILabel bestscore;
	public UILabel endscore;
	public UILabel scorelabel;
	public UILabel hplabel;
	public GameObject heart;
	public UISprite title;

	Vector3 downpos;

	int bestscorenum = 0;
	int score = 0;

	// Use this for initialization
	void Start () {

		//if(PlayerPrefs.HasKey("LanguageKey") && PlayerPrefs.GetString("LanguageKey") == "zh-Hans")
		if(Application.systemLanguage == SystemLanguage.Chinese)
		{
			title.spriteName = "title_cn";
			hint.GetComponent<UILabel>().text = "点击 或 滑动 躲避障碍";
		}

		foreach(Transform child in startgroup.transform)
		{
			if(child.GetComponent<BoxCollider>())
			{
				UIEventListener.Get(child.gameObject).onClick = OnClickButton;
			}
		}

		foreach(Transform child in pausegroup.transform)
		{
			if(child.GetComponent<BoxCollider>())
			{
				UIEventListener.Get(child.gameObject).onClick = OnClickButton;
			}
		}

		foreach(Transform child in endgroup.transform)
		{
			if(child.GetComponent<BoxCollider>())
			{
				UIEventListener.Get(child.gameObject).onClick = OnClickButton;
			}
		}

		UIEventListener.Get(pausebutton).onClick = OnClickPasueButton;
		GameObject background = gameObject.transform.FindChild("BgBoxCollider").gameObject;
		UIEventListener.Get (background).onPress = OnPressBgBoxCollider;

		if(PlayerPrefs.HasKey("BestScore"))
		{
			bestscorenum = PlayerPrefs.GetInt("BestScore");
		}

		Home();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnClickButton(GameObject button)
	{
		if(button.name == "Start")
		{
			Game.instance.GameStart();
		}
		else if(button.name == "Resume")
		{
			pausegroup.SetActive(false);
			ingamegroup.SetActive(true);
			Game.instance.Resume();
		}
		else if(button.name == "Home")
		{
			Game.instance.Home();
		}
		else if(button.name == "Sound")
		{
			Game.instance.EnableSound(!Game.instance.IsSoundEnable());
			button.GetComponent<SoundState>().UpdateState();
		}
		else if(button.name == "GameCenter")
		{
			GameCenterManager.instance.ShowLeaderboard();
		}
		else if(button.name == "Rate")
		{
			Application.OpenURL("https://itunes.apple.com/app/id953971046");
			Debug.Log("here");
		}
	}


	public void AddScore()
	{
		score += 1;
		scorelabel.text = score.ToString();
	}

	public void GameEnd()
	{
		UpdateGroup(endgroup);

		if(!PlayerPrefs.HasKey("BestScore"))
		{
			bestscorenum = score;
		}
		else
		{
			if(score > bestscorenum)
			{
				bestscorenum = score;
			}
		}

		PlayerPrefs.SetInt("BestScore", bestscorenum);
		GameCenterManager.instance.ReportScore(bestscorenum, "1");
		endscore.text = bestscorenum.ToString();
	}

	public void Home()
	{
		UpdateGroup(startgroup);
		bestscore.text = bestscorenum.ToString();
	}

	public void GameStart()
	{
		score = 0;
		scorelabel.text = "0";
		UpdateHP();

		UpdateGroup(ingamegroup);

		if(bestscorenum < 10)
		{
			hint.SetActive(true);
			hint.GetComponent<TweenAlpha>().ResetToBeginning();
			hint.GetComponent<TweenAlpha>().enabled = true;
		}
		else
		{
			hint.SetActive(false);
		}
	}


	public void UpdateHP()
	{
		hplabel.text = "X " + Game.instance.GetHP().ToString();
	}

	public void AddEffect()
	{
		heart.SetActive(true);
		
		TweenPosition tween1 = heart.GetComponent<TweenPosition>();
		tween1.ResetToBeginning();
		tween1.PlayForward();
		
		TweenAlpha tween2 = heart.GetComponent<TweenAlpha>();
		tween2.ResetToBeginning();
		tween2.PlayForward();
	}

	void OnClickPasueButton(GameObject button)
	{
		Game.instance.Pause();
	}

	void OnPressBgBoxCollider(GameObject button, bool bPress)
	{
		if(bPress)
		{
			downpos = Input.mousePosition;
		}
		else
		{
			float deltaY = Input.mousePosition.y - downpos.y;
			if(deltaY > 30)
			{
				Game.instance.MoveSnowBall(false);
			}
			else if(deltaY < -30)
			{
				Game.instance.MoveSnowBall(true);
			}
			else
			{
				Game.instance.OnTap();
			}
		}

	}

	public void Pause()
	{
		UpdateGroup(pausegroup);
	}

	void UpdateGroup(GameObject showgo)
	{
		ingamegroup.SetActive(false);
		startgroup.SetActive(false);
		endgroup.SetActive(false);
		pausegroup.SetActive(false);

		showgo.SetActive(true);

		foreach(Transform child in showgo.transform)
		{
			TweenPosition tween = child.GetComponent<TweenPosition>();
			if(tween)
			{
				tween.ResetToBeginning();
				tween.PlayForward();
			}

			TweenAlpha tweenalpha = child.GetComponent<TweenAlpha>();
			if(tweenalpha)
			{
				tweenalpha.ResetToBeginning();
				tweenalpha.PlayForward();
			}
		}
	}

}
