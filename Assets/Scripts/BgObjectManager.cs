﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BgObjectManager : MonoBehaviour 
{
	public static BgObjectManager instance;





	LinkedList <GameObject> objects = new LinkedList <GameObject> ();

	public GameObject AddObject (string name)
	{
		GameObject obj = Resources.Load <GameObject>(name).Instantiate();
		obj.transform.ChangeParent (Camera.main.transform);
		obj.transform.position = new Vector3 (0, 0, 1);
		objects.AddLast (obj);
		return obj;
	}

	public void RunObjects ()
	{
		foreach (GameObject obj in objects)
		{
			obj.transform.position += new Vector3 (-Game.instance.speed * Time.deltaTime, 0, 0);
		}
		while (objects.First != null)
		{
			GameObject rmobj = objects.First.Value;
			ObstacleLayout layout = rmobj.GetComponent <ObstacleLayout> ();
			if (rmobj.transform.position.x + layout.width < -10)
			{
				GameObject.Destroy (rmobj);
				objects.RemoveFirst ();
			}
			else
				break;
		}
	}

	public void ClearObjects ()
	{
		while (objects.First != null)
		{
			GameObject rmobj = objects.First.Value;
			GameObject.Destroy (rmobj);
			objects.RemoveFirst ();
		}
	}




	protected class ObstacleSequence
	{
		ObstacleLine[] lines;

		public ObstacleSequence (ObstacleLine[] lines)
		{
			this.lines = lines;
			Reset ();
		}

		int currentLine;

		public bool NewLine (float difficulty, ref float generatedDistance)
		{
			currentLine ++;
			if (currentLine < lines.Length)
			{
				generatedDistance += lines[currentLine].NewLine (difficulty);
				return true;
			}
			else
			{
				Reset ();
				return false;
			}
		}

		public void Reset ()
		{
			currentLine = -1;
		}
	}

	ObstacleSequence[] sequences;

	protected interface ObstacleLine
	{
		float NewLine (float difficulty);
	}

	protected class EmptyLine : ObstacleLine
	{
		public float NewLine (float difficulty)
		{
			return 1.0f;
		}
	}

	protected class Obstacle1Line : ObstacleLine
	{
		public float NewLine (float difficulty)
		{
			GameObject obj = BgObjectManager.instance.AddObject("Obstacle1");
			ObstacleLayout layout = obj.GetComponent <ObstacleLayout> ();
			Obstacle01 obj1 = obj.GetComponent<Obstacle01> ();
			Debug.Log ("Obstacle1 width = " + layout.width);
			return layout.width;
		}
	}

	protected class Obstacle2Line : ObstacleLine
	{
		public float NewLine (float difficulty)
		{
			GameObject obj = BgObjectManager.instance.AddObject("Obstacle2");
			ObstacleLayout layout = obj.GetComponent <ObstacleLayout> ();
			Obstacle02 obj2 = obj.GetComponent<Obstacle02> ();
			Debug.Log ("Obstacle2 width = " + layout.width);
			return layout.width;
		}
	}
	
	protected class Obstacle3Line : ObstacleLine
	{
		public float NewLine (float difficulty)
		{
			GameObject obj = BgObjectManager.instance.AddObject("Obstacle3");
			ObstacleLayout layout = obj.GetComponent <ObstacleLayout> ();
			Obstacle03 obj3 = obj.GetComponent<Obstacle03> ();
			Debug.Log ("Obstacle3 width = " + layout.width);
			return layout.width;
		}
	}
	
	protected class Obstacle4Line : ObstacleLine
	{
		public float NewLine (float difficulty)
		{
			GameObject obj = BgObjectManager.instance.AddObject("Obstacle4");
			ObstacleLayout layout = obj.GetComponent <ObstacleLayout> ();
			Obstacle04 obj4 = obj.GetComponent<Obstacle04> ();
			Debug.Log ("Obstacle4 width = " + layout.width);
			return layout.width;
		}
	}
	
	protected class Obstacle5Line : ObstacleLine
	{
		public float NewLine (float difficulty)
		{
			GameObject obj = BgObjectManager.instance.AddObject("Obstacle5");
			ObstacleLayout layout = obj.GetComponent <ObstacleLayout> ();
			Obstacle05 obj5 = obj.GetComponent<Obstacle05> ();
			Debug.Log ("Obstacle5 width = " + layout.width);
			return layout.width;
		}
	}
	
	void InitConfigurations ()
	{
		this.sequences = new ObstacleSequence[]
		{
			new ObstacleSequence(new ObstacleLine[] {
				new EmptyLine (),
			}),
			new ObstacleSequence(new ObstacleLine[] {
				new Obstacle1Line (),
			}),
			new ObstacleSequence(new ObstacleLine[] {
				new Obstacle2Line (),
			}),
			new ObstacleSequence(new ObstacleLine[] {
				new Obstacle3Line (),
			}),
			new ObstacleSequence(new ObstacleLine[] {
				new Obstacle4Line (),
			}),
			new ObstacleSequence(new ObstacleLine[] {
				new Obstacle5Line (),
			}),
			new ObstacleSequence(new ObstacleLine[] {
				new EmptyLine (),
			}),
		};
	}

	ObstacleSequence currentSequence;

	float generatedDistance = 0;

	void RunLineGenerator ()
	{
		float difficulty = 1.0f;
		
		distance += Game.instance.speed * Time.deltaTime;
		if (distance > generatedDistance)
		{
			if (currentSequence == null || !currentSequence.NewLine (difficulty, ref generatedDistance))
			{
				currentSequence = sequences[Game.instance.random.Next (sequences.Length)];
				currentSequence.NewLine (difficulty, ref generatedDistance);
			}
		}
	}



	bool isRunning = false;
	float distance;
	
	public void NewGame ()
	{
		if (currentSequence != null)
		{
			currentSequence.Reset ();
			currentSequence = null;
		}
		distance = 0;
		generatedDistance = 0;
		isRunning = true;
	}

	public void Pause ()
	{
		isRunning = false;
	}

	public void Resume ()
	{
		isRunning = true;
	}

	public void EndGame ()
	{
		isRunning = false;
		ClearObjects ();
	}



	// Use this for initialization
	void Start () 
	{
		instance = this;
		InitConfigurations ();
	}

	// Update is called once per frame
	void Update () 
	{
		if (!isRunning)
			return;

		RunObjects ();
		RunLineGenerator ();
	}
}
