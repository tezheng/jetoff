﻿using UnityEngine;
using System.Collections;

public class SoundEffect : MonoBehaviour 
{
	public AudioClip hit;
	public AudioClip reward;
	public AudioClip addlife;

	AudioSource audiosrc;

	public static SoundEffect instance;

	// Use this for initialization
	void Start () {
	
		instance = this;
		audiosrc = gameObject.GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayHitEffect()
	{
		if(!Game.instance.IsSoundEnable())
			return;

		audiosrc.clip = hit;
		audiosrc.Play();
	}

	public void PlayBenefitEffect()
	{
		if(!Game.instance.IsSoundEnable())
			return;

		audiosrc.clip = reward;
		audiosrc.Play();
	}

	public void PlayAddLifeEffect()
	{
		if(!Game.instance.IsSoundEnable())
			return;
		
		audiosrc.clip = addlife;
		audiosrc.Play();
	}
}
