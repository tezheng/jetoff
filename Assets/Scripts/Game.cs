using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour {

	public GameObject shadow;
	public GameObject scotty;
	public AudioSource sound;

	float lasttime = 0;
	public float speed = 0.2f;
	public System.Random random = new System.Random ();

	static public Game instance;

	public HUDPanel hud;
	public GameObject obstacle;

	bool isPlaying = false;
	
	Vector3 scottyposition;
	float scale = 1;
	int HP = 2;

	int lifecounter = 0;
	int addlifetime = 1; 
	bool bEnableSound = true;

	float generatetime = 1.0f;
	float deltatime = 0;

	enum PosState
	{
		Up = 0,
		Down = 1,
	}
	PosState curstate = PosState.Up;

	// Use this for initialization
	void Start () {
		instance = this;
		UpdateGameObject();
		Application.targetFrameRate = 60;

		scotty.GetComponent<TweenPosition>().AddOnFinished(()=>{
			scotty.transform.localPosition = scottyposition;
		});

		if(PlayerPrefs.HasKey("Sound"))
		{
			bEnableSound = PlayerPrefs.GetInt("Sound") == 1;
			sound.enabled = bEnableSound;
		}
		else
		{
			sound.enabled = true;
		}

		StopSound();
	}
	
	// Update is called once per frame
	void Update () {

		if(!isPlaying)
			return;

		lasttime += Time.deltaTime;

		deltatime += Time.deltaTime;
		if(deltatime >= generatetime)
		{
			deltatime = 0;
		}
	}

	public void OnTap()
	{
		if(!isPlaying || lasttime < 0.6f )
			return;

		TweenPosition tween1 = gameObject.GetComponent<TweenPosition>();
		tween1.ResetToBeginning();
		tween1.PlayForward();
		
		TweenPosition tween2 = scotty.GetComponent<TweenPosition>();
		tween2.from = scottyposition;
		tween2.ResetToBeginning();
		tween2.PlayForward();
		
		lasttime = 0;
	}


	void OnTriggerEnter (Collider other)
	{
		if((other.transform.position.y < -1.5f && curstate == PosState.Up) || (other.transform.position.y > -1.5f && curstate == PosState.Down))
			return;


		if(other.tag == "Obstacle")
		{
			HP -= 1;
			
			scale = 1;
			
			scotty.GetComponent<TweenPosition>().enabled = false;
			scotty.GetComponent<TweenRotation>().enabled = true;
			gameObject.GetComponent<TweenPosition>().ResetToBeginning();
			gameObject.GetComponent<TweenPosition>().enabled = false;
			
			UpdateGameObject();
			
			scotty.transform.GetChild(0).gameObject.SetActive(true);
			hud.UpdateHP();
			
			Invoke("FinishStunEffect",0.5f);
			
			if(HP <= 0)
			{
				GameEnd();
			}

			SoundEffect.instance.PlayHitEffect();
		}
		else if(other.tag == "Benefit")
		{
			AddScore();
			AddScore();

			SoundEffect.instance.PlayBenefitEffect();
		}

		other.gameObject.SetActive(false);
	}

	void FinishStunEffect()
	{
		scotty.GetComponent<TweenRotation>().enabled = false;
		scotty.transform.localEulerAngles = Vector3.zero;
		scotty.transform.GetChild(0).gameObject.SetActive(false);
	}

	public void GameEnd()
	{
		isPlaying = false;
		hud.GameEnd();

		BgObjectManager.instance.EndGame ();

		StopSound();
	}

	public void Home()
	{
		isPlaying = false;
		hud.Home();

		BgObjectManager.instance.EndGame ();

		StopSound();
	}

	public void GameStart()
	{
		GameObject.Find("bg").GetComponent<RandomBg>().ChoseRandomBg();

		HP = 2;
		lifecounter = 0;
		addlifetime = 1;

		deltatime = 0;
		generatetime = 1.0f;
		speed = 1;

		PlaySound();

		hud.GameStart();
		isPlaying = true;

		BgObjectManager.instance.NewGame ();

		MoveSnowBall(false);
		scale = 1.0f;
		FinishStunEffect();
		UpdateGameObject();

		//AdManager.instance.HideBanner();
	}

	void UpdateGameObject()
	{
		gameObject.transform.localScale = new Vector3(scale, scale, scale);
		shadow.transform.localScale = new Vector3(scale, scale, scale);
		scottyposition = new Vector3(3.382132f, scale, -3);
		if(!scotty.GetComponent<TweenPosition>().enabled)
		{
			scotty.transform.localPosition = scottyposition;
		}
	}

	public void Pause()
	{
		isPlaying = false;
		PauseSound();
		BgObjectManager.instance.Pause ();
		hud.Pause();
	}

	public void Resume()
	{
		isPlaying = true;
		PlaySound();
		BgObjectManager.instance.Resume ();
	}

	public bool IsPlaying()
	{
		return isPlaying;
	}

	public void AddScore()
	{
		hud.AddScore();

		scale = Mathf.Min(scale + 0.05f, 2.0f);

		lifecounter++;

		if(lifecounter == addlifetime * 10)
		{
			HP += 1;
			addlifetime++;
			hud.AddEffect();
			hud.UpdateHP();
			SoundEffect.instance.PlayAddLifeEffect();
			lifecounter = 0;

			generatetime = Mathf.Max(0.3f, generatetime - 0.05f);
			speed = Mathf.Min(speed+0.1f, 2);
		}

		UpdateGameObject();
	}

	public int GetHP()
	{
		return HP;
	}

	public void EnableSound(bool bEnable)
	{
		if(bEnable == bEnableSound)
			return;

		bEnableSound = bEnable;
		sound.enabled = bEnableSound;
		sound.Stop();
		PlayerPrefs.SetInt("Sound", bEnable?1:0);
	}

	public bool IsSoundEnable()
	{
		return bEnableSound;
	}

	void PlaySound()
	{
		if(!sound.enabled)
			return;

		sound.Play();
	}

	void PauseSound()
	{
		if(!sound.enabled)
			return;

		sound.Pause();
	}

	void StopSound()
	{
		if(!sound.enabled)
			return;

		sound.Stop();
	}

	void OnApplicationPause(bool pauseStatus)
	{
		if(isPlaying)
			Pause();
	}

	public void MoveSnowBall(bool bDown)
	{
		if(!isPlaying)
			return;

		PosState newstate = bDown? PosState.Down : PosState.Up;
		if(curstate == newstate)
			return;

		curstate = newstate;
		if(bDown)
		{
			gameObject.transform.parent.transform.position = new Vector3(0, -3.0f, 0);
		}
		else
		{
			gameObject.transform.parent.transform.position = new Vector3(0, -0.56f, 0);
		}

	}
}
