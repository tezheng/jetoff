using UnityEngine;
using System.Collections;

public class GameCenterManager : MonoBehaviour
{
	public static GameCenterManager instance;

	void Start ()
	{
		instance = this;
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			Social.localUser.Authenticate (success => {
				if (success) {
				} else {
				}
			});
		}
	}
	
	public void ReportScore (long score, string leaderboardID)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer) 
		{
			if (Social.localUser.authenticated) 
			{
				Social.ReportScore (score, leaderboardID, success => {
					//check if success
				});
			}
		}
	}
	
	public void ShowLeaderboard ()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer) 
		{
			if (Social.localUser.authenticated) 
			{
				Social.ShowLeaderboardUI ();
			}
		}
	}
}
