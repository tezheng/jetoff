﻿using UnityEngine;
using System.Collections;

public class ObstacleInfo : MonoBehaviour
{
	GameObject obstacle
	{
		get
		{
			return transform.GetChild(0).gameObject;
		}
	}

	public Vector2 pivot;

	public float spriteHeight = ObstacleLayout.s_WorldHeight;
	private float _height = ObstacleLayout.s_WorldHeight;
	public float height
	{
		get
		{
			return _height;
		}

		set
		{
			_height = value;
		}
	}

	void Start()
	{
	
	}
	
	void Update()
	{
	
	}
}
