﻿
using UnityEngine;
using System.Collections;

public class Obstacle02: ObstacleLayout
{
	public GameObject upModle;
	public GameObject groundModel;
	public GameObject middleModel;
	
	public float upHeight = 8.0f;
	public float groundHeight = 4.0f;
	public float middleHeight = 12.0f;
	public float gap = 4.0f;
	
	private ObstacleInfo up;
	private ObstacleInfo ground;
	private ObstacleInfo middle;
	private float assetWidth;

	void Awake()
	{
		up = gameObject.FindChildWithNamePrefix("up_").GetComponent<ObstacleInfo>();
		ground = gameObject.FindChildWithNamePrefix("ground_").GetComponent<ObstacleInfo>();
		middle = gameObject.FindChildWithNamePrefix("middle_").GetComponent<ObstacleInfo>();

		var upm = upModle.Instantiate();
		upm.ChangeParent(up.gameObject);
		var groundm = groundModel.Instantiate();
		groundm.ChangeParent(ground.gameObject);
		var middlem = middleModel.Instantiate();
		middlem.ChangeParent(middle.gameObject);

		float left_width = Mathf.Max(upm.GetComponent<SpriteSize>().width, groundm.GetComponent<SpriteSize>().width);
		float right_width = middlem.GetComponent<SpriteSize>().width;
		assetWidth = left_width + right_width;

		Update();
	}
	
	void Update()
	{
		up.height = upHeight;
		ground.height = groundHeight;
		middle.height = middleHeight;
		middle.pivot.x = gap;
	}
	
	protected override float getWidth()
	{
		return gap + assetWidth * 0.5f;
	}
	
	protected override float getHeight()
	{
		return s_WorldHeight;
	}
}
