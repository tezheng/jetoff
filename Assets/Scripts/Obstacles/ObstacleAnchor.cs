﻿using UnityEngine;
using System.Collections;

public class ObstacleAnchor : MonoBehaviour
{
	public enum Type
	{
		UP		= 1,
		MIDDLE	= 2,
		GROUND	= 3,
		FREE	= 4,
	}

	public Type anchor = Type.FREE;

	Transform trans
	{
		get
		{
			return transform.GetChild(0).gameObject.transform;
		}
	}

	ObstacleInfo info
	{
		get
		{
			return GetComponent<ObstacleInfo>();
		}
	}

	void Update()
	{
		switch (anchor)
		{
		case Type.UP:
		{
			Vector3 newPos = info.pivot;
			newPos.y = (ObstacleLayout.s_WorldHeight - info.height) - ObstacleLayout.s_WorldHeight * 0.5f;
			trans.localPosition = newPos;
			break;
		}
		case Type.MIDDLE:
		{
			Vector3 newPos = info.pivot;
			newPos.y = 0;
			trans.localPosition = newPos;
			break;
		}
		case Type.GROUND:
		{
			Vector3 newPos = info.pivot;
			newPos.y = info.height - ObstacleLayout.s_WorldHeight * 0.5f;
			trans.localPosition = newPos;
			break;
		}
		case Type.FREE:
		{
			// Vector2 => Vector3, (x, y) => (x, y, 0)
			trans.localPosition = info.pivot;
			break;
		}
		default:
			break;
		}
	}
}
