﻿using UnityEngine;
using System.Collections;

public class Obstacle05: ObstacleLayout
{
	public GameObject upModle;
	public GameObject groundModel;
	public GameObject middleModel;
	
	public float upHeight = 2.0f;
	public float groundHeight = 4.0f;
	public float middlePivotX = 0.0f;
	public float middlePivotY = 9.0f;

	private ObstacleInfo up;
	private ObstacleInfo ground;
	private ObstacleInfo middle;
	private float assetWidth;

	void Awake()
	{
		up = gameObject.FindChildWithNamePrefix("up_").GetComponent<ObstacleInfo>();
		ground = gameObject.FindChildWithNamePrefix("ground_").GetComponent<ObstacleInfo>();
		middle = gameObject.FindChildWithNamePrefix("middle_").GetComponent<ObstacleInfo>();
		
		upModle.Instantiate().ChangeParent(up.gameObject);
		groundModel.Instantiate().ChangeParent(ground.gameObject);
		middleModel.Instantiate().ChangeParent(middle.gameObject);

		var upm = upModle.Instantiate();
		upm.ChangeParent(up.gameObject);
		var groundm = groundModel.Instantiate();
		groundm.ChangeParent(ground.gameObject);
		var middlem = middleModel.Instantiate();
		middlem.ChangeParent(middle.gameObject);

		assetWidth = Mathf.Max(upm.GetComponent<SpriteSize>().width, groundm.GetComponent<SpriteSize>().width);
		assetWidth = Mathf.Max(assetWidth, middlem.GetComponent<SpriteSize>().width);

		Update();
	}
	
	void Update()
	{
		up.height = upHeight;
		ground.height = groundHeight;
		middle.pivot.x = middlePivotX;
		middle.pivot.y = middlePivotY;
	}
	
	protected override float getWidth()
	{
		return assetWidth + 2.0f;;
	}
	
	protected override float getHeight()
	{
		return s_WorldHeight;
	}
}
