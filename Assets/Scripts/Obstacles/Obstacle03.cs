﻿
using UnityEngine;
using System.Collections;

public class Obstacle03: ObstacleLayout
{
	public GameObject upModle;
	public GameObject groundModel;

	public float upHeight = 9.0f;
	public float groundHeight = 9.0f;
	public float gap = 6.0f;
	
	private ObstacleInfo up;
	private ObstacleInfo ground;
	private float assetWidth;

	void Awake()
	{
		up = gameObject.FindChildWithNamePrefix("up_").GetComponent<ObstacleInfo>();
		ground = gameObject.FindChildWithNamePrefix("ground_").GetComponent<ObstacleInfo>();

		var upm = upModle.Instantiate();
		upm.ChangeParent(up.gameObject);
		var groundm = groundModel.Instantiate();
		groundm.ChangeParent(ground.gameObject);

		assetWidth = upm.GetComponent<SpriteSize>().width + groundm.GetComponent<SpriteSize>().width;

		Update();
	}
	
	void Update()
	{
		up.height = upHeight;
		ground.height = groundHeight;
		ground.pivot.x = gap;
	}
	
	protected override float getWidth()
	{
		return gap + assetWidth * 0.5f;
	}
	
	protected override float getHeight()
	{
		return s_WorldHeight;
	}
}
