﻿using UnityEngine;
using System.Collections;

public class Obstacle01: ObstacleLayout
{
	public GameObject upModle;
	public GameObject groundModel;
	public GameObject middleModel;

	public float upHeight = 8.0f;
	public float groundHeight = 4.0f;

	private ObstacleInfo up;
	private ObstacleInfo ground;
	private ObstacleInfo middle;
	private float assetWidth;

	void Awake()
	{
		up = gameObject.FindChildWithNamePrefix("up_").GetComponent<ObstacleInfo>();
		ground = gameObject.FindChildWithNamePrefix("ground_").GetComponent<ObstacleInfo>();
		middle = gameObject.FindChildWithNamePrefix("middle_").GetComponent<ObstacleInfo>();

		var upm = upModle.Instantiate();
		upm.ChangeParent(up.gameObject);
		var groundm = groundModel.Instantiate();
		groundm.ChangeParent(ground.gameObject);
		var middlem = middleModel.Instantiate();
		middlem.ChangeParent(middle.gameObject);

		assetWidth = Mathf.Max(upm.GetComponent<SpriteSize>().width, groundm.GetComponent<SpriteSize>().width);
		assetWidth = Mathf.Max(assetWidth, middlem.GetComponent<SpriteSize>().width);

		Update();
	}
	
	void Update()
	{
		up.height = upHeight;
		ground.height = groundHeight;
		middle.height = s_WorldHeight - up.height - ground.height;
		middle.pivot.y = ground.height + middle.height * 0.5f;
	}

	protected override float getWidth()
	{
		return 2.0f + assetWidth;
	}
	
	protected override float getHeight()
	{
		return s_WorldHeight;
	}
}
