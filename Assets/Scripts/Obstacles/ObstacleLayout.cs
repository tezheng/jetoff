﻿using UnityEngine;
using System.Collections;

public class ObstacleLayout : MonoBehaviour
{
	public static float s_WorldHeight = 16.0f;

	public float width
	{
		get
		{
			return getWidth();
		}
	}

	public float height
	{
		get
		{
			return getHeight();
		}
	}

	protected virtual float getWidth()
	{
		return 3.0f;
	}

	protected virtual float getHeight()
	{
		return s_WorldHeight;
	}

	public virtual void ReLayout()
	{
	}
}
