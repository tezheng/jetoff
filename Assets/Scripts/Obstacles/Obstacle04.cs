﻿
using UnityEngine;
using System.Collections;

public class Obstacle04: ObstacleLayout
{
	public GameObject upModle;
	public GameObject groundModel;

	public int groupCount = 5;
	public float[] upHeight;
	public float[] groundHeight;
	public float gap = 6.0f;
	
	private ObstacleInfo[] ups;
	private ObstacleInfo[] grounds;
	private float assetWidth;

	void Awake()
	{
		ups = new ObstacleInfo[groupCount];
		grounds = new ObstacleInfo[groupCount];

		for (int i = 0; i < groupCount; ++i)
		{
			ups[i] = gameObject.FindChildWithNamePrefix((i+1).ToString() + "up_").GetComponent<ObstacleInfo>();
			grounds[i] = gameObject.FindChildWithNamePrefix((i+1).ToString() + "ground_").GetComponent<ObstacleInfo>();

			var upm = upModle.Instantiate();
			upm.ChangeParent(ups[i].gameObject);
			var groundm = groundModel.Instantiate();
			groundm.ChangeParent(grounds[i].gameObject);

//			assetWidth += Mathf.Max(upm.GetComponent<SpriteSize>().width, groundm.GetComponent<SpriteSize>().width);
		}

//		assetWidth += (groupCount - 1) * gap;

		for (int i = groupCount; i < 9; ++i)
		{
			GameObject.Destroy(gameObject.FindChildWithNamePrefix((i+1).ToString() + "up_"));
			GameObject.Destroy(gameObject.FindChildWithNamePrefix((i+1).ToString() + "ground_"));
		}

		ReLayout();
	}
	
	void Update()
	{
		ReLayout();
	}

	public override void ReLayout()
	{
		for (int i = 0; i < groupCount; ++i)
		{
			ups[i].height = upHeight[i];
			ups[i].pivot.x = i * gap;
			grounds[i].height = groundHeight[i];
			grounds[i].pivot.x = i * gap;
		}
	}

	protected override float getWidth()
	{
		return groupCount * gap;
	}
	
	protected override float getHeight()
	{
		return s_WorldHeight;
	}
}
