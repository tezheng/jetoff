using UnityEngine;
using System.Collections;

public class TerrainController : MonoBehaviour
{
	Vector3 start;
	Vector3 end;
	Vector3 speed;

	public float widthFactor;
	public float speedFactor;
	
	// Use this for initialization
	void Start () {

		start = new Vector3(2 * widthFactor, transform.position.y, transform.position.z);
		end = new Vector3(-widthFactor, transform.position.y, transform.position.z);
		speed = new Vector3(-speedFactor, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		
		if(!Game.instance.IsPlaying())
			return;
		
		gameObject.transform.localPosition += speed * Game.instance.speed * Time.deltaTime;
		
		if(gameObject.transform.localPosition.x > end.x)
			return;

		gameObject.transform.localPosition = start;
	}

}

